"use client";

import DialogConfirmDelete from "@/components/globals/DialogConfirmDelete";
import { useToast } from "@/components/ui/use-toast";
import { deletePost, fetchPostById } from "@/services/post.service";
import { useMutation, useQuery } from "@tanstack/react-query";
import { useParams, useRouter } from "next/navigation";
import DrawerPost from "@/components/post/Update/DrawerPost";

type PostDetailParams = {
  id: string;
};

const PostDetail = () => {
  const { id } = useParams<PostDetailParams>();
  const router = useRouter();
  const { toast } = useToast();

  const { isPending, error, data } = useQuery({
    queryKey: ["PostData" + id],
    queryFn: () => fetchPostById(id),
  });

  const mutation = useMutation({
    mutationFn: deletePost,
    onSuccess: () => {
      toast({
        title: "Post deleted",
        description: "Your post has been deleted",
      });
      router.push("/");
    },
  });

  const handleDelete = () => {
    mutation.mutate(id);
  };

  if (isPending)
    return (
      <div className="h-full flex justify-center items-center">Loading...</div>
    );

  return (
    <div className="px-10">
      <h2 className="text-4xl font-bold my-5 text-cyan-700 mb-2">
        {data.title}
      </h2>
      <p>Description : {data.description}</p>
      <p>Category : {data.category?.name}</p>

      <DialogConfirmDelete
        handleDelete={handleDelete}
        isPending={mutation.isPending}
      />
      <DrawerPost
        categoryId={data.category?.id}
        description={data.description}
        postId={data.id}
        title={data.title}
      />
    </div>
  );
};

export default PostDetail;
