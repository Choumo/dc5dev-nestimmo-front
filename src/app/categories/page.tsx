import CategoryList from "@/components/category/CategoryList";

export default function Categories() {
    return (
        <div className="px-10">
            <CategoryList />
        </div>
    );
}