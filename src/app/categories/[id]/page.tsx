"use client";

import DialogConfirmDelete from "@/components/globals/DialogConfirmDelete";
import { useToast } from "@/components/ui/use-toast";
import { useMutation, useQuery } from "@tanstack/react-query";
import { useParams, useRouter } from "next/navigation";
import { deleteCategory, fetchCategoryById } from "@/services/category.service";
import Link from "next/link";
import {
  Card,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import DrawerCategory from "@/components/category/Update/DrawerCategory";

type CategoryDetailParams = {
  id: string;
};

const CategoryDetail = () => {
  const { id } = useParams<CategoryDetailParams>();
  const router = useRouter();
  const { toast } = useToast();

  const { isPending, error, data } = useQuery({
    queryKey: ["CategoryData" + id],
    queryFn: () => fetchCategoryById(id),
  });

  const mutation = useMutation({
    mutationFn: deleteCategory,
    onSuccess: () => {
      toast({
        title: "Category deleted",
        description: "Your category has been deleted",
      });
      router.push("/categories");
    },
  });

  const handleDelete = () => {
    mutation.mutate(id);
  };

  if (isPending)
    return (
      <div className="h-full flex justify-center items-center">Loading...</div>
    );

  return (
    <div className="px-10">
      <h2 className="text-4xl font-bold my-5 text-cyan-700 mb-2">
        {data.name}
      </h2>

      <DialogConfirmDelete
        handleDelete={handleDelete}
        isPending={mutation.isPending}
      />
      <DrawerCategory categoryId={data.id} initialName={data.name} />
      <div className="grid grid-cols-4 gap-2 mt-5">
        {data.posts?.map((post: any) => (
          <Link key={post.id} href={`/posts/${post.id}`}>
            <Card>
              <CardHeader>
                <CardTitle>{post.title}</CardTitle>
                <CardDescription>{post.description}</CardDescription>
              </CardHeader>
              <CardFooter>
                <p>{post.category?.name}</p>
              </CardFooter>
            </Card>
          </Link>
        ))}
      </div>
    </div>
  );
};

export default CategoryDetail;
