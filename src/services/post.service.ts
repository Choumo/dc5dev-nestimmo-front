import { PostCreateDTO, PostUpdateDTO } from "@/types/post";
import { POST_ENDPOINT } from "@/utils/constants";

export const fetchAllPosts = async () => {
  const response = await fetch(POST_ENDPOINT);
  return await response.json();
};

export const fetchPostById = async (id: string) => {
  const response = await fetch(`${POST_ENDPOINT}/${id}`);
  return await response.json();
};

export const createPost = async (createPostDTO: PostCreateDTO) => {
  const response = await fetch(POST_ENDPOINT, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(createPostDTO),
  });
  return await response.json();
};

export const updatePost = async (updatePostDTO: PostUpdateDTO) => {
  const response = await fetch(`${POST_ENDPOINT}/${updatePostDTO.id}`, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(updatePostDTO),
  });

  return await response.json();
};

export const deletePost = async (id: string) => {
  const response = await fetch(`${POST_ENDPOINT}/${id}`, {
    method: "DELETE",
  });
  return await response.json();
};
