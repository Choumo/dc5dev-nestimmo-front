"use client";

import {
  Drawer,
  DrawerClose,
  DrawerContent,
  DrawerDescription,
  DrawerFooter,
  DrawerHeader,
  DrawerTitle,
  DrawerTrigger,
} from "@/components/ui/drawer";

import { Button } from "@/components/ui/button";
import FormPost from "./FormPost";
import { useState } from "react";

type FormDrawerProps = {
  postId: number;
  title: string;
  description: string;
  categoryId: string | null;
};

const DrawerPost = ({
  postId,
  title,
  description,
  categoryId,
}: FormDrawerProps) => {
  const [open, setOpen] = useState(false);

  return (
    <Drawer open={open} onOpenChange={setOpen}>
      <DrawerTrigger asChild>
        <Button variant="default">Update post</Button>
      </DrawerTrigger>
      <DrawerContent>
        <DrawerHeader>
          <DrawerTitle className="text-center">Modifier un post</DrawerTitle>
          <DrawerDescription className="text-center">
            Renseignez l'ensemble des champs.
          </DrawerDescription>
          <FormPost
            setOpen={setOpen}
            categoryId={categoryId}
            description={description}
            postId={postId}
            title={title}
          />
        </DrawerHeader>
        <DrawerFooter>
          <DrawerClose>
            <Button variant="outline">Cancel</Button>
          </DrawerClose>
        </DrawerFooter>
      </DrawerContent>
    </Drawer>
  );
};

export default DrawerPost;
