"use client";

import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import {
  createCategory,
  updateCategoryById,
} from "@/services/category.service";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { CategoryCreateDTO, CategoryUpdateDTO } from "@/types/category";
import React from "react";

type FormCategoryProps = {
  setOpen: (open: boolean) => void;
  initialName: string;
  categoryId: number;
};

const FormCategory = ({
  setOpen,
  categoryId,
  initialName,
}: FormCategoryProps) => {
  const queryClient = useQueryClient();

  const mutation = useMutation({
    mutationFn: updateCategoryById,
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: ["getAllCategories"],
      });
      queryClient.invalidateQueries({
        queryKey: ["CategoryData" + categoryId],
      });
      setOpen(false);
    },
  });

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const updateCateforyDTO: CategoryUpdateDTO = {
      id: categoryId,
      // @ts-ignore
      name: e.target.name.value,
    };

    mutation.mutate(updateCateforyDTO);
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="mb-2">
        <Input
          type="text"
          placeholder="Category name"
          defaultValue={initialName}
          name="name"
        />
      </div>
      <div>
        <Button type="submit" className="w-full" disabled={mutation.isPending}>
          {mutation.isPending && (
            <span className="mr-4 h-4 w-4 rounded-full bg-white animate-pulse"></span>
          )}
          Update category
        </Button>
      </div>
    </form>
  );
};

export default FormCategory;
