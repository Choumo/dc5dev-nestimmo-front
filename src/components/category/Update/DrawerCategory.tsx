"use client";

import {
  Drawer,
  DrawerClose,
  DrawerContent,
  DrawerDescription,
  DrawerFooter,
  DrawerHeader,
  DrawerTitle,
  DrawerTrigger,
} from "@/components/ui/drawer";

import { Button } from "@/components/ui/button";
import FormCategory from "./FormCategory";
import { useState } from "react";

type DrawerCategoryProps = {
  initialName: string;
  categoryId: number;
};

const DrawerCategory = ({ categoryId, initialName }: DrawerCategoryProps) => {
  const [open, setOpen] = useState(false);

  return (
    <Drawer open={open} onOpenChange={setOpen}>
      <DrawerTrigger asChild>
        <Button variant="default">Update this category</Button>
      </DrawerTrigger>
      <DrawerContent>
        <DrawerHeader>
          <DrawerTitle className="text-center">
            Ajouter une catégorie
          </DrawerTitle>
          {/* eslint-disable-next-line react/no-unescaped-entities */}
          <DrawerDescription className="text-center">
            Renseignez l'ensemble des champs.
          </DrawerDescription>
          <FormCategory
            setOpen={setOpen}
            categoryId={categoryId}
            initialName={initialName}
          />
        </DrawerHeader>
        <DrawerFooter>
          <DrawerClose>
            <Button variant="outline">Cancel</Button>
          </DrawerClose>
        </DrawerFooter>
      </DrawerContent>
    </Drawer>
  );
};

export default DrawerCategory;
