export interface PostCreateDTO {
  title: string;
  description: string;
  category: string;
}

export interface PostUpdateDTO {
  id: number;
  title: string;
  description: string;
  category: string;
}
