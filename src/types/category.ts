export interface CategoryCreateDTO {
  name: string;
}

export interface CategoryUpdateDTO {
  id: number;
  name: string;
}
